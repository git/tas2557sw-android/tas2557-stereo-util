@ECHO OFF
:: Default options
set ADB=adb.exe
set RCCD2=rccd2
set PORT=80
set SAFE=
set CARD=0
set LOGCAT=OFF
set DEBUG=OFF

goto GETOPTS

:USAGE
echo.
echo.   rccd2.bat  -- Start RCCD server on target Android phone
echo.
echo.   Usage: rccd2 [/help] [/safe] [/supersafe] [/card:N] [/port:N] 
echo.                     [/adb:ADB] [/rccd2:RCCD2] [/logcat]
echo.
echo.   Options:
echo.
echo.       /help           Display this help text and exit
echo.       /port:N         Set port number to N (Default: %PORT%)
echo.       /card:N         Select sound card N (Default: %CARD%)
echo.       /adb:ADB        Use ADB as the adb executable
echo.                       (Default: %ADB%)
echo.       /rccd2:RCCD2    Use RCCD2 as the target rccd2 executable
echo.                       (Default: %RCCD2%)
echo.       /logcat         Start adb logcat at the end
echo.       /debug          Output debug information
echo.
goto END


:GETOPTS
    set SWITCHOPT=%1
    if [%SWITCHOPT%] == [] goto START

    for /F "tokens=1,2 delims=: " %%a IN ("%SWITCHOPT%") DO SET SWITCH=%%a&set VALUE=%%b
   
    shift

    if [%SWITCH%] == [/h] goto USAGE
    if [%SWITCH%] == [/H] goto USAGE
    if [%SWITCH%] == [/?] goto USAGE
    if [%SWITCH%] == [/help] goto USAGE
    if [%SWITCH%] == [/HELP] goto USAGE

    if [%SWITCH%] == [/safe] goto SET_SAFE_ON
    if [%SWITCH%] == [/SAFE] goto SET_SAFE_ON
    if [%SWITCH%] == [/supersafe] goto SET_SSAFE_ON
    if [%SWITCH%] == [/SUPERSAFE] goto SET_SSAFE_ON

    if [%SWITCH%] == [/card] goto SET_CARD
    if [%SWITCH%] == [/CARD] goto SET_CARD

    if [%SWITCH%] == [/port] goto SET_PORT
    if [%SWITCH%] == [/PORT] goto SET_PORT

    if [%SWITCH%] == [/adb] goto SET_ADB
    if [%SWITCH%] == [/ADB] goto SET_ADB

    if [%SWITCH%] == [/rccd2] goto SET_RCCD2
    if [%SWITCH%] == [/RCCD2] goto SET_RCCD2

    if [%SWITCH%] == [/logcat] goto SET_LOGCAT_ON
    if [%SWITCH%] == [/LOGCAT] goto SET_LOGCAT_ON

    if [%SWITCH%] == [/debug] goto SET_DEBUG_ON
    if [%SWITCH%] == [/DEBUG] goto SET_DEBUG_ON


    echo. Unknown option %SWITCH%. Ignored
goto GETOPTS


:SET_SAFE_ON
    set SAFE=-s
goto GETOPTS

:SET_SSAFE_ON
    set SAFE=-S
goto GETOPTS

:SET_PORT
    set PORT=%VALUE%
goto GETOPTS

:SET_CARD
    set CARD=%VALUE%
goto GETOPTS

:SET_ADB
    set ADB=%VALUE%
goto GETOPTS

:SET_RCCD2
    set RCCD2=%VALUE%
goto GETOPTS

:SET_LOGCAT_ON
    set LOGCAT=ON
goto GETOPTS


:SET_DEBUG_ON
    REM echo ON
    set DEBUG=ON
goto GETOPTS

:START

if [%DEBUG%] == [OFF] goto RUN
    echo. "Starting RCCD..."
    echo. SAFE=%SAFE%
    echo. PORT=%PORT%
    echo. CARD=%CARD%
    echo. ADB=%ADB%
    echo. RCCD2=%RCCD2%
    echo. LOGCAT=%LOGCAT%

:RUN

%ADB% root
%ADB% remount
%ADB% shell svc power stayon true
ping localhost > NUL
%ADB% push %RCCD2% /system/bin/rccd2
%ADB% shell chmod 0777 /system/bin/rccd2
%ADB% forward tcp:%PORT% tcp:%PORT%
%ADB% shell /system/bin/rccd2 -k
%ADB% shell /system/bin/rccd2 -p %PORT% -r %CARD% %SAFE%

if [%DEBUG%] == [ON] echo "RCCD2 is running..."

if [%LOGCAT%] == [OFF] goto END
    %ADB% logcat -s aic3xxx

:END


