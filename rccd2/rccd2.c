#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <time.h>
#include <getopt.h>
#include "utils.h"
#include "android_utils.h"
#include "rccd2.h"
#include "aic3xxx_cfw.h"

/* Max connection requests */
#define MAXPENDING      (16)
#define BUFFSIZE        (256)
#define DEFAULT_PORT    (80)

/* State of each independent connection */
struct rccd_hdl {
	u32 pcounter;
	char fwid[32];
	int clientsock;
	int fCtrl;
	pthread_mutex_t *mutex;
	int slave_id,book, page;
	u8 buffer[RCCD_MAX_PACKET];
	struct cfw_block block;
};

#define CRITICAL(code_section__) do {           \
if (pthread_mutex_lock(pc->mutex))          \
	DBGF("Error obtaining mutex lock");     \
	\
	code_section__;                             \
	\
if (pthread_mutex_unlock(pc->mutex))        \
	DBGF("Error releasing mutex lock");     \
} while (0)

void legacy_read(struct rccd_hdl *pc, u8 *buf, int n)
{
	pc->block.ncmds = CFW_CMD_BURST_LEN(n);
	pc->block.cmd[0].bhdr.cid = CFW_CMD_RBURST;
	pc->block.cmd[0].bhdr.len = n;
	//pc->block.cmd[1].burst.slave_id = pc->slave_id;
	pc->block.cmd[1].burst.book = pc->book;
	pc->block.cmd[1].burst.page = pc->page;
	pc->block.cmd[1].burst.offset = buf[0];
	DBG("legacy_read: (%d,%d,%d) %d bytes", pc->book, pc->page, buf[0], n);
	CRITICAL(run_block(&(pc->block), NULL));
	memcpy(buf + 1, pc->block.cmd[1].burst.data, n);
}

void legacy_write(struct rccd_hdl *pc, u8 *buf, int n)
{
	u8 *pd = buf;
	if (buf[0] == 127) {
		pc->book = buf[1];
		if (n != 2)
			DBGW("Received book change as part of burst command.  "
			"Additional writes have been ignored (n=%d)", n);
		return;
	}
	if (buf[0] == 0) {
		pc->page = buf[1];
		--n;
		if (n <= 1)
			return;
		++pd;
		pd[0] = 1;
	}
	pc->block.ncmds = CFW_CMD_BURST_LEN(n - 1);
	pc->block.cmd[0].bhdr.cid = CFW_CMD_BURST;
	pc->block.cmd[0].bhdr.len = n - 1;
	//pc->block.cmd[1].burst.slave_id = pc->slave_id;
	pc->block.cmd[1].burst.book = pc->book;
	pc->block.cmd[1].burst.page = pc->page;
	pc->block.cmd[1].burst.offset = pd[0];
	memcpy(pc->block.cmd[1].burst.data, &pd[1], n - 1);
	DBG("legacy_write: (%d,%d,%d) %d bytes", pc->book, pc->page, pd[0], n - 1);
	CRITICAL(run_block(&(pc->block), NULL));
}

int rccd_cmd(struct rccd_hdl *pc, struct rccd_cmd *cmd, int len)
{
	DBG("Getting into rccd_cmd");
	const char * const cid_str[] = {
		[RCCD_CMD_READ] = "RCCD_CMD_READ",
		[RCCD_CMD_WRITE] = "RCCD_CMD_WRITE",
		[RCCD_CMD_SLAVE] = "RCCD_CMD_SLAVE",
		[RCCD_CMD_FWID] = "RCCD_CMD_FWID",
		[RCCD_CMD_BLOCK] = "RCCD_CMD_BLOCK",
		[RCCD_CMD_OPEN] = "RCCD_CMD_OPEN",
		[RCCD_CMD_CLOSE] = "RCCD_CMD_CLOSE",
		[RCCD_CMD_PLAY] = "RCCD_CMD_PLAY",
		[RCCD_CMD_CAPTURE] = "RCCD_CMD_CAPTURE",
		[RCCD_CMD_START] = "RCCD_CMD_START",
		[RCCD_CMD_STOP] = "RCCD_CMD_STOP",
		[RCCD_CMD_MGET] = "RCCD_CMD_MGET",
		[RCCD_CMD_MSET] = "RCCD_CMD_MSET",
		[RCCD_CMD_NCTLS] = "RCCD_CMD_NCTLS",
		[RCCD_CMD_CTL] = "RCCD_CMD_CTL",
	};

	DBG("%s: cid=%0x \"%s\"  len=%d", __FUNCTION__, cmd->cid, cid_str[cmd->cid],
		cmd->len);

	//if (!pc->mixer && cmd->cid > RCCD_CMD_BLOCK)
	//	DBGF("Attempt to do ALSA operation when no sound card is present");

	switch (cmd->cid) {
	case RCCD_CMD_READ:
		DBG("rccd:RCCD_CMD_READ");
		if (cmd->len < 4)
			break;
		legacy_read(pc, cmd->data, cmd->len - 4);
		break;
	case RCCD_CMD_WRITE:
		DBG("rccd:RCCD_CMD_WRITE");
		if (cmd->len < 4)
			break;
		legacy_write(pc, cmd->data, cmd->len - 4 + 1);
		break;
	case RCCD_CMD_BLOCK:
	{
		DBG("rccd:RCCD_CMD_BLOCK");
		u8 var[256];
		struct cfw_block *b = (void *)cmd->data;
		len = CFW_BLOCK_SIZE(b->ncmds) + sizeof(struct rccd_cmd);
		CRITICAL(run_block(b, var));
	}
		break;
	case RCCD_CMD_OPEN:
		DBG("rccd:RCCD_CMD_OPEN - : NOT SUPPORTED");
		break;
	case RCCD_CMD_CLOSE:
		DBG("rccd:RCCD_CMD_CLOSE - : NOT SUPPORTED");
		break;
	case RCCD_CMD_PLAY:
		DBG("rccd:RCCD_CMD_PLAY - : NOT SUPPORTED");
		break;

	case RCCD_CMD_CAPTURE:
		DBG("rccd:RCCD_CMD_CAPTURE - : NOT SUPPORTED");
		break;

	case RCCD_CMD_START:
		DBG("rccd:RCCD_CMD_START - : NOT SUPPORTED");
		break;

	case RCCD_CMD_STOP:
		DBG("rccd:RCCD_CMD_STOP - : NOT SUPPORTED");
		break;
	case RCCD_CMD_MGET:
		DBG("rccd:RCCD_CMD_MGET - : NOT SUPPORTED");
		break;

	case RCCD_CMD_MSET:
		DBG("rccd:RCCD_CMD_MSET - : NOT SUPPORTED");
		break;

	case RCCD_CMD_NCTLS:
		DBG("rccd:RCCD_CMD_NCTLS - : NOT SUPPORTED");
		break;

	case RCCD_CMD_CTL:
		DBG("rccd:RCCD_CMD_CTL - : NOT SUPPORTED");
		break;

	case RCCD_CMD_FWID:
	{
		DBG("rccd:RCCD_CMD_FWID");
		//u32 addr = (cmd->data[1] << 8) | (cmd->data[2]);
		strcpy((char *)(cmd->data + 3), pc->fwid);
	}
		break;
	case RCCD_CMD_SLAVE:
	{
		u8 slave = cmd->data[0];
		int client = slave;
		ioctl(pc->fCtrl, _IOW(0xE0, 5, int), &client);
		DBG("Setting slave : 0x%x", slave);
	}
		break;
	default:
		DBGW("HandleI2CCmd: unsupported command: %d",
			cmd->cid);
	}
	DBG("Getting out of rccd_cmd");
	return len;
}

void dump_pkt(u8 *b, int nLength)
{
	int i;
	DBG("Packet[length=%d]", nLength);
	for (i = 0; i < nLength; i += 8) {
		switch (nLength - i) {
		case 1: DBG(" 0x%02x", b[i + 0]); break;
		case 2: DBG(" 0x%02x 0x%02x", b[i + 0], b[i + 1]); break;
		case 3: DBG(" 0x%02x 0x%02x 0x%02x", b[i + 0], b[i + 1], b[i + 2]); break;
		case 4: DBG(" 0x%02x 0x%02x 0x%02x 0x%02x", b[i + 0], b[i + 1], b[i + 2], b[i + 3]); break;
		case 5: DBG(" 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", b[i + 0], b[i + 1], b[i + 2], b[i + 3], b[i + 4]); break;
		case 6: DBG(" 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", b[i + 0], b[i + 1], b[i + 2], b[i + 3], b[i + 4], b[i + 5]); break;
		case 7: DBG(" 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", b[i + 0], b[i + 1], b[i + 2], b[i + 3], b[i + 4], b[i + 5], b[i + 6]); break;
		default:DBG(" 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", b[i + 0], b[i + 1], b[i + 2], b[i + 3], b[i + 4], b[i + 5], b[i + 6], b[i + 7]); break;
		}
	}
}

void write_pkt(u8 *b, int nLength, char *filePath)
{
	int i;
	int output_fd, ret_out;
	char buffer[512];

	output_fd = open(filePath, O_RDWR | O_APPEND | O_CREAT, 0644);

	sprintf(buffer, "Packet[length=%d]%c", nLength,'\0');
	ret_out = write(output_fd, &buffer, strlen(buffer));
	for (i = 0; i < nLength; i += 8) {
		switch (nLength - i) {
		case 1: sprintf(buffer, " 0x%02x%c", b[i + 0],'\0'); break;
		case 2: sprintf(buffer, " 0x%02x 0x%02x%c", b[i + 0], b[i + 1],'\0'); break;
		case 3: sprintf(buffer, " 0x%02x 0x%02x 0x%02x%c", b[i + 0], b[i + 1], b[i + 2],'\0'); break;
		case 4: sprintf(buffer, " 0x%02x 0x%02x 0x%02x 0x%02x%c", b[i + 0], b[i + 1], b[i + 2], b[i + 3],'\0'); break;
		case 5: sprintf(buffer, " 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x%c", b[i + 0], b[i + 1], b[i + 2], b[i + 3], b[i + 4],'\0'); break;
		case 6: sprintf(buffer, " 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x%c", b[i + 0], b[i + 1], b[i + 2], b[i + 3], b[i + 4], b[i + 5], '\0'); break;
		case 7: sprintf(buffer, " 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x%c", b[i + 0], b[i + 1], b[i + 2], b[i + 3], b[i + 4], b[i + 5], b[i + 6], '\0'); break;
		default:sprintf(buffer, " 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x%c", b[i + 0], b[i + 1], b[i + 2], b[i + 3], b[i + 4], b[i + 5], b[i + 6], b[i + 7], '\0'); break;
		}
		ret_out = write(output_fd, &buffer, strlen(buffer));
	}
	sprintf(buffer, "\n%c",'\0');
	ret_out = write(output_fd, &buffer, strlen(buffer));
	close(output_fd);
}

static int get_packet(struct rccd_hdl *pc, u8 *p, int max, int *is_ping)
{
	int ret;
	int sock = pc->clientsock;
	if ((ret = recv(sock, p, max, 0)) <= 0)
		goto err;
	struct rccd_request *preq = (void *)p;
	++pc->pcounter;
	DBG("[%8d] Received PKT %d bytes", pc->pcounter, ret);
	dump_pkt(p, ret);
	write_pkt(p, ret, "/sdcard/RCCD2/bytes.txt");
	if (preq->target != TARGET_I2C)
		DBGF("Unsupported Target ID: %d\n", preq->target);
	if (preq->len < 8) {
		DBGW("Packet too short");
		goto err;
	}

	DBG("MAGIC %s", preq->magic);
	if (!memcmp(preq->magic, RCCD_PING, sizeof(preq->magic)))
	{
		DBG("PING... ");
		*is_ping = 1;
		return ret;
	}

	if (memcmp(preq->magic, RCCD_MAGIC, sizeof(preq->magic))) {
		DBGW("Incorrect signature: %c%c%c%c", preq->magic[0],
			preq->magic[1], preq->magic[2], preq->magic[3]);
		goto err;
	}
	if (!(preq->cmd[0].cid&RCCD_CMD_BLOCK) &&
		(ret != (int)(preq->len + strlen(RCCD_MAGIC)))) {
		DBGW("Incorrect total length: %d,%d for cid=%02x", preq->len, ret, preq->cmd[0].cid);
		goto err;
	}

	if (preq->cmd[0].cid < RCCD_CMD_BLOCK)
		return ret;

	int sz = sizeof(struct rccd_request);
	DBG("[%d] rccd_request size = %d\n", pc->pcounter,sz);
	struct rccd_cmd_alsa *a = (void *)(preq->cmd);
	struct cfw_block *b = (void *)(preq->cmd[0].data);
	switch (preq->cmd[0].cid) {
	case RCCD_CMD_BLOCK:
		sz += sizeof(struct rccd_cmd) + CFW_BLOCK_SIZE(b->ncmds);
		DBG("[%d] cid = RCCD_CMD_BLOCK size = %d\n", pc->pcounter,sz);
		break;
	case RCCD_CMD_OPEN:
	case RCCD_CMD_CLOSE:
	case RCCD_CMD_START:
	case RCCD_CMD_STOP:
	case RCCD_CMD_CAPTURE:
		sz += sizeof(struct rccd_cmd_alsa);
		DBG("[%d] cid = RCCD_CMD_CAPTURE size = %d\n", pc->pcounter,sz);
		break;
	case RCCD_CMD_PLAY:
		sz += sizeof(struct rccd_cmd_alsa) + a->buffer_size;
		DBG("[%d] cid = RCCD_CMD_PLAY size = %d\n", pc->pcounter,sz);
		break;
	case RCCD_CMD_MGET:
	case RCCD_CMD_MSET:
	case RCCD_CMD_NCTLS:
	case RCCD_CMD_CTL:
		sz += sizeof(struct rccd_cmd_alsa) + 256 * 2;
		DBG("[%d] cid = RCCD_CMD_MGET size = %d\n", pc->pcounter,sz);
		break;
	default:
		DBGW("Unknown command cid=%02x", preq->cmd[0].cid);
		goto err;
	}
	if (sz > max)
		goto err;
	p += ret;
	DBG("[%d] Got %d out of %d bytes", pc->pcounter, ret, sz);
	
#if 0	
	while (ret < sz) {
		DBG("\t%d bytes left", sz - ret);
		int rsz;
		if ((rsz = recv(sock, p, sz - ret, 0)) <= 0)
			goto err;
		ret += rsz;
	}
#endif	
	return ret;
err:
	if (ret < 0)
		return ret;
	return 0;
}

void *handle_client(void *p)
{
	struct rccd_hdl *pc = p;
	int sock = pc->clientsock;
	u8 *buffer = pc->buffer;
	int n;

	DBGI("Client thread started");
	int is_ping = 0;

	/* Receive message */
	while ((n = get_packet(pc, buffer, RCCD_MAX_PACKET, &is_ping)) > 0)
	{

		struct rccd_request *preq = (void *)buffer;
		int off = offsetof(struct rccd_request, cmd);
		if (is_ping == 0){
			DBGI("Processing Packet");

			int c = rccd_cmd(pc, (struct rccd_cmd *)(buffer + off), n - off);
			send(sock, buffer, off + c, 0);
		}
		else
		{
			DBGI("Pinging ..");
			int len = sizeof(preq->magic);
			struct rccd_cmd *cmd = (struct rccd_cmd *)(buffer + off);
			memcpy(&(cmd->data[1]), preq->magic, len);
			send(sock, buffer, off + len, 0);
		}
		is_ping = 0;
	}
	close(sock);

	DBGI("Client disconnected");
	
	free(pc);
	return NULL;
}

void sig_exit(int sig)
{
	DBGW("SIGTERM received. Exiting.0x%x",sig);
	exit(0);
}

void daemonize(char *const argv[])
{
	int i, err, pid;
	DBGI("%s", argv[0]);
	if (getppid() == 1) {
		DBGI("RCCD2 running in background (PID %d)", getpid());
		/* already a daemon */
		return;
	}
	for (;;) {
		if ((pid = fork()) < 0)
			DBGF("Fork error");
		if (pid <= 0)
			break;
		// Parent
		sleep(1);
		if ((err = waitpid(pid, NULL, WNOHANG)) == 0) {
			DBGW("Parent: Fork successful.  Exiting");
			exit(0);
		}
		if (err < 0) {
			DBGF("Unknown error");
		}

		DBGW("Android killed my baby! Baby's ID = %d",pid);
	}
	if (setsid() < 0)
		error("Unable to daemonize");
	DBGI("Child running %d", getpid());
	setsid();
	DBGI("setsid done");
	/* close standard I/O */
	for (i = 0; i <= 2; ++i)
		close(i);
	if (0 != open("/dev/null", O_RDWR))
		DBGF("Unexpected error");
	dup(0); dup(0);
	DBG("Closed all descriptors");


	signal(SIGCHLD, SIG_IGN);
	signal(SIGTSTP, SIG_IGN);
	signal(SIGTTOU, SIG_IGN);
	signal(SIGTTIN, SIG_IGN);
	signal(SIGHUP, SIG_IGN);
	/* catch kill signal */
	signal(SIGTERM, sig_exit);
}

int read_pid(char *dname)
{
	int ret;
	char *e, *b = strrchr(dname, '/');
	if (b)
		b++;
	else
		b = dname;
	ret = strtol(b, &e, 10);
	if (*e)
		return -1;
	return ret;
}

int find_server(void)
{
	DIR *fd;
	struct dirent *fdf;
	static int found = 0;
	int  pid;
	char ln[64];
	FILE *fp;

	if (!(fd = opendir("/proc")))
		error("Unable to open /proc");
	while ((fdf = readdir(fd))) {
		if ((pid = read_pid(fdf->d_name)) < 0)
			continue;
		if (pid == getpid())
			continue;
		sprintf(ln, "/proc/%d/cmdline", pid);
		if (!(fp = fopen(ln, "rb")))
			continue;
		if (!fgets(ln, sizeof(ln), fp)) {
			fclose(fp);
			continue;
		}
		if (strstr(ln, "rccd2")) {
			DBGW("Found rccd process with pid %d", pid);
			++found;
			return pid;
		}
	}
	closedir(fd);
	DBGI("No %srunning rccd2 processes found", found ? "more " : "");
	return 0;
}

void kill_server(void)
{
	int pid;

	while ((pid = find_server())) {
		DBGW("Killing rccd process with pid %d", pid);
		kill(pid, SIGTERM);
	}
	exit(0);
}

const struct option options[] = {
	{ "help", 0, 0, 'h' },
	{ "verbose", 1, 0, 'v' },
	{ "device", 1, 0, 'd' },
	{ "codec-device", 0, 0, 'c' },
	{ "port", 1, 0, 'p' },
	{ "kill-server", 0, 0, 'k' },
	{ "safe-mode", 0, 0, 's' },
	{ "super-safe", 0, 0, 'S' },
	{ "dump", 0, 0, 'D' },
	{ 0, 0, 0, 0 }
};

void usage(int ret)
{
	FILE *f = stdout;
	if (ret)
		f = stderr;
	fprintf(f,
		"Usage: rccd2 [OPTIONS]\n"
		"        Start the Remote CodecControl Server Daemon\n"
		"Options:\n"
		"    --help, -h              Print this help\n"
		"    --verbose=N, -v N       Set verbose level\n"
		"    --safe-mode, -s         Use safe mode (do not fork())\n"
		"    --super-safe, -S        Play super safe.\n"
		"                               No fork(), no concurrent connections\n"
		"    --port=N, -p N          Port number on which to listen\n"
		"                                (Default: %d)\n"
		"    --kill-server, -k       Kill any running instance of rccd2\n"
		"    --dump, -D              Provide a log of commands as they are executed.\n"
		"    --device=<dev>          Open <dev> as the device node\n"
		"        -d <dev>\n"
		"    --fwid=<id>, -f <id>    Set firmware id\n"
		"                                (Default: \"%s\")\n"
		"\n"
		"    [See \"acxrun -h\" for additional help on \"--device\" and]\n"
		"    [\"--codec-device\" options.                              ]\n",
		DEFAULT_PORT, DEFAULT_FWID);
	exit(ret);
}

int main(int argc, char *argv[])
{
	int serversock, clientsock;
	struct sockaddr_in echoserver, echoclient;
	int fCtrl, i = 0, opt, safe = 0, card = 0;
	int port = DEFAULT_PORT;
	char *device = NULL, *fwid = DEFAULT_FWID;
	char *opts = make_opts(options);
	pthread_mutex_t *mutex = ALLOC(sizeof(pthread_mutex_t));
	pthread_mutex_init(mutex, NULL);

	DBGI("[DMSG] %s pid=%d ppid=%d", argv[0], getpid(), getppid());
	while ((opt = getopt_long(argc, argv, opts, options, &i)) != -1) {
		switch (opt) {
		case 'h':
			usage(0);
			break;
		case 'v':
			verbose = atoi(optarg);
			break;
		case 'd':
			device = strdup(optarg);
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'k':
			kill_server();
			break;
		case 's':
			safe = 1;
			break;
		case 'D':
			dump_cmds = 1;
			break;
		case 'S':
			safe = 2;
			break;
		case 'r':
			card = atoi(optarg);
			break;
		case 'f':
			fwid = strdup(optarg);
			break;
		default:
			usage(1);
		}
	}
	while (optind < argc)
		DBGW("Extra command line argument \"%s\".  Ignored", argv[optind++]);
	
	if (getppid() != 1 && (i = find_server()))
		DBGF("RCCD2 is already running (pid %d)", i);

	if (!safe) {
		daemonize(argv);
		DBG("Running as daemon");
	}

	DBGW("Opening Device");
	//vishnu
	fCtrl = open_device(device);
	
	/* Create the TCP socket */
	if ((serversock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		DBGF("Failed to create socket");
	/* Construct the server sockaddr_in structure */
	/* Clear struct */
	memset(&echoserver, 0, sizeof(echoserver));
	/* Internet/IP */
	echoserver.sin_family = AF_INET;
	/* Incoming addr */
	echoserver.sin_addr.s_addr = htonl(INADDR_ANY);
	/* server port */
	echoserver.sin_port = htons(port);

	/* Bind the server socket */
	DBGW("Bind Server Socket");
	if (bind(serversock, (struct sockaddr *)&echoserver, sizeof(echoserver)) < 0)
		DBGF("Failed to bind the server socket");

	/* Listen on the server socket */
	if (listen(serversock, MAXPENDING) < 0)
		DBGF("Failed to listen on server socket");

	/* Run until cancelled */
	while (1) {
		socklen_t clientlen = sizeof(echoclient);
		DBGI("Waiting for client connection...");
		/* Wait for client connection */
		if ((clientsock = accept(serversock, (struct sockaddr *) &echoclient,
			&clientlen)) < 0)
			DBGF("Failed to accept client connection");
		DBGI("Client connected: %s", inet_ntoa(echoclient.sin_addr));
		struct rccd_hdl *p = ALLOC(sizeof(struct rccd_hdl) + sizeof(union cfw_cmd)*BUFFSIZE);
		pthread_t tid;
		strcpy(p->fwid, fwid);
		p->clientsock = clientsock;
		p->fCtrl = fCtrl;
		p->mutex = mutex;
		/* Pass mutex  as well */ 
		if (safe > 1)
		{
			DBG("Entering Safe Mode");
			handle_client(p);
		}
		else
		{
			DBG("Starting in thread");

			if (pthread_create(&tid, NULL, handle_client, p) != 0)
				DBGF("Error creating thread");
		}
	}
}
