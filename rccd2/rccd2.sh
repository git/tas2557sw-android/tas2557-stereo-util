#!/bin/sh

ADB=adb
RCCD2=rccd2
PORT=5000
CARD=0
DEBUG=1
LOGCAT=1
SAFE=""
DEBUG_OPTS=

usage()  {
    echo "usage goes here"
    exit 0
}

while getopts "p:c:a:r:dlsh" opt; do
    case $opt in
        h) usage ;;
        p) PORT=$OPTARG ;;
        c) CARD=$OPTARG ;;
        a) ADB=$OPTARG ;;
        r) RCCD2=$OPTARG ;;
        d) DEBUG=1 ; DEBUG_OPTS="-v 10";;
        l) LOGCAT=1 ;;
        s) SAFE=-s ;;
    esac
done

if [ "$DEBUG" -eq "1" ]
then
    echo "PORT=$PORT"
    echo "CARD=$CARD"
    echo "ADB=$ADB"
    echo "RCCD2=$RCCD2"
    echo "SAFE=$SAFE"
    echo "LOGCAT=$LOGCAT"
fi

${ADB} root
${ADB} remount
${ADB} shell svc power stayon true
sleep 3
${ADB} push "$RCCD2" /system/bin/rccd2
${ADB} shell chmod 0777 /system/bin/rccd2
${ADB} forward tcp:${PORT} tcp:${PORT}
${ADB} shell rccd2 -k
${ADB} shell rccd2 --port=${PORT} --card=${CARD} ${SAFE} ${DEBUG_OPTS} -v 4
if [ "$DEBUG" -eq "1" ]
then
    echo "RCCD2 is running..."
fi

if [ "$LOGCAT" -eq "1" ]
then
    ${ADB} logcat -s aic3xxx
fi


