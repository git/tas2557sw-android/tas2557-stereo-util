#ifndef ANDROID_UTILS_H_
#define ANDROID_UTILS_H_

#include "host_compile.h"

#ifdef ANDROID
#include <android/log.h>
#include <cutils/log.h>
#else

typedef enum android_LogPriority {
    ANDROID_LOG_UNKNOWN = 0,
    ANDROID_LOG_DEFAULT,    /* only for SetMinPriority() */
    ANDROID_LOG_VERBOSE,
    ANDROID_LOG_DEBUG,
    ANDROID_LOG_INFO,
    ANDROID_LOG_WARN,
    ANDROID_LOG_ERROR,
    ANDROID_LOG_FATAL,
    ANDROID_LOG_SILENT,     /* only for SetMinPriority(); must be last */
} android_LogPriority;

#endif

/* TAS MACROS */
#define TAS2560_REG(book,page,reg)	(((unsigned int)book * 256 *128) + ((unsigned int)page * 128) + reg)
#define TAS2560_BOOK_ID(reg)		((unsigned char)(reg / (256*128)))
#define TAS2560_PAGE_ID(reg)		((unsigned char)((reg % (256 * 128)) / 128))
#define TAS2560_REG_ID(reg)			((unsigned char)((reg % (256 * 128)) % 128))

int open_device(char *device);
void diag(int lvl, char *fmt, ...);
extern int dump_cmds;
enum { mode_undef, mode_user, mode_kernel };
extern int mode;

#ifndef PROGRAM
#   define PROGRAM "aic3xxx"
#endif


#undef DBG
#undef DBGF

#define DBGD(fmt, ...)  do { \
                            if (dump_cmds) \
                                LOG(ANDROID_LOG_DEBUG, fmt, ##__VA_ARGS__); \
                        } while (0)
#define DBG(fmt, ...)   LOG(ANDROID_LOG_DEBUG, "[%s:%d] " fmt, __FILE__, \
                                            __LINE__, ##__VA_ARGS__)
#define DBGI(fmt, ...)  LOG(ANDROID_LOG_INFO, fmt, ##__VA_ARGS__)
#define DBGW(fmt, ...)  LOG(ANDROID_LOG_WARN, fmt, ##__VA_ARGS__)
#define DBGF(fmt, ...)  LOG(ANDROID_LOG_FATAL, fmt, ##__VA_ARGS__)
#define LOG(lvl, fmt, ...) diag(lvl, fmt, ##__VA_ARGS__)
#endif
